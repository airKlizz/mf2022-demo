from elg import Service

service = Service.from_id(18092, auth_file="offline_tokens.json")
print(service)
result = service("This is a text in English to translate.", output_func="auto")
print("Translation in Ukrainian: ", result)