function translateText(languages) {
  fetch(`http://localhost:8000/${languages}`, {
    method: "POST",
    body: JSON.stringify({
      content: document.getElementById(`input-${languages}`).value,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      return Promise.reject(response);
    })
    .then(function (data) {
      console.log(data);
      document.getElementById(`translation-${languages}`).innerHTML =
        data.translation;
    })
    .catch(function (error) {
      console.warn("Something went wrong.", error);
      document.getElementById(
        languages
      ).innerHTML = `Something went wrong. ${error}`;
    });
}
