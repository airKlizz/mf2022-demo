from fastapi import FastAPI, Request, HTTPException
from elg import Service

from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

translation_services = {
    "en-uk": Service.from_id(18092, auth_file="offline_tokens.json"),
    "uk-en": Service.from_id(18110, auth_file="offline_tokens.json"),
    "en-ru": Service.from_id(18091, auth_file="offline_tokens.json"),
    "ru-en": Service.from_id(18110, auth_file="offline_tokens.json"),
}

@app.post("/{languages}")
async def process(languages: str, request: Request):
    service = translation_services[languages] 
    request_content = await request.json()
    text_request = request_content["content"]
    try:
        response = service(text_request, sync_mode=True, timeout=300, verbose=False, check_file=False, output_func="auto")
    except Exception as e:
        raise HTTPException(status_code=404, detail=f"Issue during the service call: {e}")
    return {"translation": response}
