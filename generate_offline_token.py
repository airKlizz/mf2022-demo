from elg import Authentication

auth = Authentication.init(scope="offline_access")
auth.to_json("offline_tokens.json")